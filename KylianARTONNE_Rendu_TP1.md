```bash
sudo vi /etc/sysconfig/network-scripts/ifcfg-ens224
sudo setenforce 0
sudo vi /etc/selinux/config
sudo nmcli con reload
sudo nmcli con up ens224
ping google.com

# PING google.com (216.58.198.206) 56(84) bytes of data.

# 64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=1 ttl=128 time=17.1 ms

# 64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=2 ttl=128 time=42.8 ms

# 64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=3 ttl=128 time=24.3 ms

# --- google.com ping statistics ---

# 3 packets transmitted, 3 received, 0% packet loss, time 2005ms

# rtt min/avg/max/mdev = 17.106/28.068/42.759/10.799 ms
```
```bash
sudo vi /etc/hostname
adduser adminka -d /home/adminka -G wheel
sudo systemctl status sshd
sudo ss -lutpn < Port sshd : 42069
sudo vi /etc/ssh/sshd_config
sudo systemctl restart sshd
sudo firewall-cmd --add-port=42069/tcp --permanent
sudo firewall-cmd --reload
sudo ss -ltnp
sudo dnf install nginx
# Complete!
systemctl start nginx
# ==== AUTHENTICATION COMPLETE ====
sudo ss -lutpn 
# Netid             State              Recv-Q              Send-Q                           Local Address:Port                            Peer Address:Port             Process
# tcp               LISTEN             0                   128                                    0.0.0.0:80                                   0.0.0.0:*                 users:(("nginx",pid=26341,fd=8),("nginx",pid=26340,fd=8))
# tcp               LISTEN             0                   128                                    0.0.0.0:42069                                0.0.0.0:*                 users:(("sshd",pid=2077,fd=4))
# tcp               LISTEN             0                   128                                       [::]:80                                      [::]:*                 users:(("nginx",pid=26341,fd=9),("nginx",pid=26340,fd=9))
# tcp               LISTEN             0                   128                                       [::]:42069                                   [::]:*                 users:(("sshd",pid=2077,fd=6))
sudo firewall-cmd --add-port=80/tcp --permanent
sudo firewall-cmd --reload
```
```bash
curl 10.0.0.200
# <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

# <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
#   <head>
#     <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
#     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
#     <style type="text/css">
#       /*<![CDATA[*/
#       body {
#         background-color: #fff;
#         color: #000;
#         font-size: 0.9em;
#         font-family: sans-serif, helvetica;
#         margin: 0;
#         padding: 0;
#       }
#       :link {
#         color: #c00;
#       }
#       :visited {
#         color: #c00;
#       }
#       a:hover {
#         color: #f50;
#      }
#       h1 {
#         text-align: center;
#         margin: 0;
#         padding: 0.6em 2em 0.4em;
#         background-color: #10B981;
#         color: #fff;
#         font-weight: normal;
#         font-size: 1.75em;
#         border-bottom: 2px solid #000;
#       }
#       h1 strong {
#         font-weight: bold;
#         font-size: 1.5em;
#       }
#       h2 {
#         text-align: center;
#         background-color: #10B981;
#         font-size: 1.1em;
#         font-weight: bold;
#         color: #fff;
#         margin: 0;
#         padding: 0.5em;
#         border-bottom: 2px solid #000;
#       }
#       hr {
#         display: none;
#       }
#       .content {
#         padding: 1em 5em;
#       }
#       .alert {
#         border: 2px solid #000;
#       }
# 
#       img {
#         border: 2px solid #fff;
#         padding: 2px;
#         margin: 2px;
#       }
#       a:hover img {
#         border: 2px solid #294172;
#       }
#       .logos {
#         margin: 1em;
#         text-align: center;
#       }
#       /*]]>*/
#     </style>
#   </head>
# 
#   <body>
#     <h1>Welcome to <strong>nginx</strong> on Rocky Linux!</h1>
# 
#     <div class="content">
#       <p>
#         This page is used to test the proper operation of the
#         <strong>nginx</strong> HTTP server after it has been installed. If you
#         can read this page, it means that the web server installed at this site
#         is working properly.
#       </p>
# 
#       <div class="alert">
#         <h2>Website Administrator</h2>
#         <div class="content">
#           <p>
#             This is the default <tt>index.html</tt> page that is distributed
#             with <strong>nginx</strong> on Rocky Linux. It is located in
#             <tt>/usr/share/nginx/html</tt>.
#           </p>
# 
#           <p>
#             You should now put your content in a location of your choice and
#             edit the <tt>root</tt> configuration directive in the
#             <strong>nginx</strong>
#             configuration file
#             <tt>/etc/nginx/nginx.conf</tt>.
#           </p>
# 
#           <p>
#             For information on Rocky Linux, please visit the
#             <a href="https://www.rockylinux.org/">Rocky Linux website</a>. The
#             documentation for Rocky Linux is
#             <a href="https://www.rockylinux.org/"
#               >available on the Rocky Linux website</a
#             >.
#           </p>
#         </div>
#       </div>
# 
#       <div class="logos">
#         <a href="http://nginx.net/"
#           ><img
#             src="nginx-logo.png"
#             alt="[ Powered by nginx ]"
#             width="121"
#             height="32"
#         /></a>
#         <a href="http://www.rockylinux.org/"><img
#             src="poweredby.png"
#             alt="[ Powered by Rocky Linux ]"
#             width="88" height="31" /></a>
# 
#       </div>
#     </div>
#   </body>
# </html>
```
```bash
sudo dnf install python3
sudo vi /etc/systemd/system/serverpython.service
sudo systemctl daemon-reload
sudo systemctl start serverpython
sudo systemctl status serverpython
# Active: active (running) since Mon 2021-12-06 13:59:58 CET; 43s ago
sudo systemctl enable serverpython
sudo adduser web
sudo mkdir /srv/web/
sudo chown web /srv/web/index
ls -al /srv/web/index

# -rw-r--r--. 1 web root 21 Dec  6 14:05 /srv/web/index

sudo vi /etc/systemd/system/serverpython.service
sudo systemctl daemon-reload
sudo systemctl restart serverpython
sudo firewall-cmd --add-port=8888/tcp --permanent
sudo firewall-cmd --reload
```
