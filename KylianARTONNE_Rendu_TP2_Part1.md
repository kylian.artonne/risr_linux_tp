VM web
-------------------------------------------------------------------------------------------------------------------
```bash 
sudo vi /etc/sysconfig/network-scripts/ifcfg-ens224
> TYPE=ethernet
> DEVICE=ens224
> NAME=ens224
> BOOTPROTO=static
> ONBOOT=yes
> IPADDR=10.0.0.221
> NETMASK=255.255.255.0
> DNS1=1.1.1.1
sudo setenforce 0
sudo vi /etc/selinux/config
sudo nmcli con reload
sudo nmcli con up ens224
> Connection successfully activated
ping google.com
> PING google.com (142.250.179.78) 56(84) bytes of data.
> 64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=128 time=21.3 ms
> 64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=128 time=51.0 ms
> 64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=128 time=19.1 ms
> --- google.com ping statistics ---
> 3 packets transmitted, 3 received, 0% packet loss, time 2003ms
> rtt min/avg/max/mdev = 19.084/30.471/51.003/14.548 ms
sudo vi /etc/hostname
> web.tp2.cesi
```
-------------------------------------------------------------------------------------------------------------------
VM db
-------------------------------------------------------------------------------------------------------------------
```bash
sudo vi /etc/sysconfig/network-scripts/ifcfg-ens224
> TYPE=ethernet
> DEVICE=ens224
> NAME=ens224
> BOOTPROTO=static
> ONBOOT=yes
> IPADDR=10.0.0.222
> NETMASK=255.255.255.0
> DNS1=1.1.1.1
sudo setenforce 0
sudo vi /etc/selinux/config
sudo nmcli con reload
sudo nmcli con up ens224
> Connection successfully activated
ping google.com
> PING google.com (172.217.19.238) 56(84) bytes of data.
> 64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=1 ttl=128 time=21.4 ms
> 64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=2 ttl=128 time=16.9 ms
> 64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=3 ttl=128 time=16.8 ms
> --- google.com ping statistics ---
> 3 packets transmitted, 3 received, 0% packet loss, time 2003ms
> rtt min/avg/max/mdev = 16.758/18.350/21.395/2.159 ms
sudo vi /etc/hostname
> db.tp2.cesi
```
-------------------------------------------------------------------------------------------------------------------
```bash
[kylian@db ~]$ sudo dnf install mariadb-server
Last metadata expiration check: 0:03:18 ago on Tue 07 Dec 2021 02:11:46 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                      Architecture  Version                                           Repository        Size
=====================================================================================================================
Installing:
 mariadb-server               x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707           appstream         16 M
[...]
Complete!
[kylian@db ~]$ sudo systemctl enable mariadb
*Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[kylian@db ~]$ sudo systemctl start mariadb
[sudo] password for kylian:
[kylian@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:21:21 CET; 2s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 27127 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 26992 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 26968 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 27095 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11221)
   Memory: 81.4M
   CGroup: /system.slice/mariadb.service
           └─27095 /usr/libexec/mysqld --basedir=/usr

Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: See the MariaDB Knowledgebase at http://mariadb.com/kb or t>
Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: MySQL manual for more instructions.
Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: Please report any problems at http://mariadb.org/jira
Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: The latest information about MariaDB is available at http:/>
Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: You can find additional information about the MySQL part at:
Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: http://dev.mysql.com
Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: Consider joining MariaDB's strong and vibrant community:
Dec 07 14:21:21 db.tp2.cesi mysql-prepare-db-dir[26992]: https://mariadb.org/get-involved/
Dec 07 14:21:21 db.tp2.cesi mysqld[27095]: 2021-12-07 14:21:21 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-MariaDB)>
Dec 07 14:21:21 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.
[kylian@db ~]$ sudo ss -lutpn
Netid   State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port  Process
tcp     LISTEN   0        128              0.0.0.0:22            0.0.0.0:*      users:(("sshd",pid=995,fd=5))
tcp     LISTEN   0        80                     *:3306                *:*      users:(("mysqld",pid=27095,fd=21))
tcp     LISTEN   0        128                 [::]:22               [::]:*      users:(("sshd",pid=995,fd=7))
[kylian@db ~]$ ps
    PID TTY          TIME CMD
   1675 pts/0    00:00:00 bash
  27182 pts/0    00:00:00 ps
[kylian@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[kylian@db ~]$ sudo firewall-cmd --reload
success
[kylian@db ~]$ sudo mysql_secure_installation
[...]
Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
[kylian@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 19
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
MariaDB [(none)]> CREATE USER 'kylian'@'10.0.0.222' IDENTIFIED BY 'kylian';
Query OK, 0 rows affected (0.000 sec)
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS kylian CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)
MariaDB [(none)]> GRANT ALL PRIVILEGES ON kylian.* TO 'kylian'@'10.0.0.222';
Query OK, 0 rows affected (0.000 sec)
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
-------------------------------------------------------------------------------------------------------------------
VM web
-------------------------------------------------------------------------------------------------------------------
```bash
[kylian@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                                            592 kB/s | 8.3 MB     00:14
Rocky Linux 8 - BaseOS                                                               977 kB/s | 3.5 MB     00:03
Rocky Linux 8 - Extras                                                               8.4 kB/s |  10 kB     00:01
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
[kylian@web ~]$ sudo dnf install mysql -y
Rocky Linux 8 - AppStream                                                            1.1 MB/s | 8.3 MB     00:07
Rocky Linux 8 - BaseOS                                                               967 kB/s | 3.5 MB     00:03
Rocky Linux 8 - Extras                                                               8.4 kB/s |  10 kB     00:01
[...]
Complete!
[kylian@web ~]$ mysql -h 10.0.0.222 -P 3306 -u kylian -p kylian
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 33
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
mysql> SHOW TABLES;
Empty set (0.00 sec)
[kylian@web ~]$ sudo dnf install httpd -y
[sudo] password for kylian:
Last metadata expiration check: 0:00:10 ago on Tue 07 Dec 2021 03:11:32 PM CET.
Dependencies resolved.
Complete!
[kylian@web ~]$ sudo systemctl enable httpd
[kylian@web ~]$ sudo systemctl start httpd
[kylian@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:13:36 CET; 26s ago
     Docs: man:httpd.service(8)
 Main PID: 24635 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 11221)
   Memory: 25.4M
   CGroup: /system.slice/httpd.service
           ├─24635 /usr/sbin/httpd -DFOREGROUND
           ├─24636 /usr/sbin/httpd -DFOREGROUND
           ├─24637 /usr/sbin/httpd -DFOREGROUND
           ├─24638 /usr/sbin/httpd -DFOREGROUND
           └─24639 /usr/sbin/httpd -DFOREGROUND

Dec 07 15:13:36 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 15:13:36 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 15:13:36 web.tp2.cesi httpd[24635]: Server configured, listening on: port 80
[kylian@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[kylian@web ~]$ sudo firewall-cmd --reload
success
```
-------------------------------------------------------------------------------------------------------------------
PC
-------------------------------------------------------------------------------------------------------------------
```bash
C:\Users\kylianartonne>curl 10.0.0.221
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
[...]
  </body>
</html>
```
-------------------------------------------------------------------------------------------------------------------
VM web
-------------------------------------------------------------------------------------------------------------------
```bash
[kylian@web ~]$ sudo dnf install epel-release
Last metadata expiration check: 0:05:48 ago on Tue 07 Dec 2021 03:11:32 PM CET.
Dependencies resolved.
[...]
Complete!
[kylian@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                       3.1 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64                               961 kB/s | 980 kB     00:01
[...]
Complete!
[kylian@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:02:44 ago on Tue 07 Dec 2021 03:18:28 PM CET.
remi-release-8.rpm
[...]
Complete!
[kylian@web ~]$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64                            2.9 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                            3.0 MB/s | 3.1 kB     00:00
[...]
Complete!
[kylian@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:00:56 ago on Tue 07 Dec 2021 03:22:00 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
[...]
Complete!
[kylian@web ~]$ ls /etc/httpd/conf.d/
autoindex.conf  php74-php.conf  README  userdir.conf  welcome.conf
[kylian@web ~]$ sudo vi /etc/httpd/conf.d/nextcloud.conf
[sudo] password for kylian:
<VirtualHost *:80>
 [...]
</VirtualHost>
[kylian@web ~]$ sudo mkdir /var/www/nextcloud
[kylian@web ~]$ sudo mkdir /var/www/nextcloud/html
[kylian@web ~]$ sudo chown apache:apache /var/www/nextcloud/html
[kylian@web ~]$ timedatectl
               Local time: Tue 2021-12-07 15:42:56 CET
           Universal time: Tue 2021-12-07 14:42:56 UTC
                 RTC time: Tue 2021-12-07 14:42:56
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no
[kylian@web ~]$ sudo vi /etc/opt/remi/php74/php.ini
	date.timezone = "Europe/Paris"
[kylian@web ~]$ cd
[kylian@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  4631k      0  0:00:32  0:00:32 --:--:-- 3130k
[kylian@web ~]$ ls
nextcloud-21.0.1.zip
[kylian@web ~]$ unzip nextcloud-21.0.1.zip
[kylian@web ~]$ sudo mv nextcloud /var/www/nextcloud/html/		  
[kylian@web ~]$ sudo rm nextcloud-21.0.1.zip
[kylian@web ~]$ sudo mv /var/www/nextcloud/html/nextcloud/* /var/www/nextcloud/html/
[kylian@web ~]$ sudo systemctl restart httpd		
[kylian@web ~]$ sudo chown apache:apache /var/www/nextcloud/html -R
```
-------------------------------------------------------------------------------------------------------------------
PC
-------------------------------------------------------------------------------------------------------------------
```bash
C:\Users\kylianartonne>curl http://web.tp2.cesi/index.php
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 [...]
        </body>
</html>
```







