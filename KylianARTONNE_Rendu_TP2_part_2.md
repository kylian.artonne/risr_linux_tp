PC 
-------------------------------------------------------------------------------------------------------------------
```bash
PS C:\Users\kylianartonne> scp 'C:\Users\kylianartonne\.ssh\id_rsa.pub' kylian@10.0.0.221:/home/kylian/
kylian@10.0.0.221's password:
id_rsa.pub                                                                100%  758   775.2KB/s   00:00
PS C:\Users\kylianartonne> scp 'C:\Users\kylianartonne\.ssh\id_rsa.pub' kylian@10.0.0.222:/home/kylian/
kylian@10.0.0.222's password:
id_rsa.pub                                                                100%  758   372.2KB/s   00:00
```
-------------------------------------------------------------------------------------------------------------------
VM web
-------------------------------------------------------------------------------------------------------------------
```bash
[kylian@web ~]$ mkdir /home/kylian/.ssh
[kylian@web ~]$ mv /home/kylian/id_rsa.pub  /home/kylian/.ssh/authorized_keys
[kylian@web ~]$ chmod -R 700 /home/kylian/.ssh/
[kylian@web ~]$ chmod 600 /home/kylian/.ssh/authorized_keys
[kylian@web ~]$ sudo vi /etc/ssh/sshd_config
PermitRootLogin no
PasswordAuthentication no
```
-------------------------------------------------------------------------------------------------------------------
PC
-------------------------------------------------------------------------------------------------------------------
```bash
PS C:\Users\kylianartonne> ssh kylian@10.0.0.221
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Dec  8 09:49:19 2021 from 10.0.0.1
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Dec  8 09:49:19 2021 from 10.0.0.1
```
-------------------------------------------------------------------------------------------------------------------
VM web
-------------------------------------------------------------------------------------------------------------------
```bash
[kylian@web ~]$ sudo systemctl start firewalld
[kylian@web ~]$ sudo systemctl enable firewalld
[kylian@web ~]$ sudo dnf install epel-release -y
Last metadata expiration check: 19:00:52 ago on Tue 07 Dec 2021 03:22:00 PM CET.
Package epel-release-8-13.el8.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
[kylian@web ~]$ sudo dnf install fail2ban fail2ban-firewalld -y
Last metadata expiration check: 19:02:12 ago on Tue 07 Dec 2021 03:22:00 PM CET.
Dependencies resolved.
[...]
Complete!
[kylian@web ~]$ sudo systemctl start fail2ban
[kylian@web ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[kylian@web ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
[kylian@web ~]$ sudo vi /etc/fail2ban/jail.local
bantime = 1h
findtime = 1h
maxretry = 5
[kylian@web ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
[kylian@web ~]$ sudo systemctl restart fail2ban
[kylian@web ~]$ sudo vi /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true
bantime = 1d
maxretry = 3
[kylian@web ~]$ sudo systemctl restart fail2ban
```
-------------------------------------------------------------------------------------------------------------------
VM proxy
-------------------------------------------------------------------------------------------------------------------
```bash
[kylian@proxy ~]$ sudo dnf install nginx
[sudo] password for kylian:
Last metadata expiration check: 19:14:28 ago on Tue 07 Dec 2021 03:22:00 PM CET.
Dependencies resolved.
[...]
Complete!
[kylian@proxy ~]$ sudo vi /etc/nginx/conf.d/r_proxy.conf
server {
    listen 80;
    server_name web.tp2.cesi;

 location/ {
    proxy_pass http://web.tp2.cesi;
    proxy_http_version  1.1;
    proxy_cache_bypass  $http_upgrade;

    proxy_set_header Upgrade           $http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              $host;
    proxy_set_header X-Real-IP         $remote_addr;
    proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host  $host;
    proxy_set_header X-Forwarded-Port  $server_port;
  }
}
[kylian@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[kylian@web ~]$ sudo firewall-cmd --reload
success
[kylian@proxy ~]$ sudo vi /etc/hosts
[kylian@proxy ~]$ sudo systemctl start nginx
[kylian@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 13:53:26 CET; 2s ago
 [...]
 (j'ai bien l'accés au site)
 [kylian@proxy ~]$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
Country Name (2 letter code) [XX]:fr
State or Province Name (full name) []:Bordeaux
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:RISR
Common Name (eg, your name or your server's hostname) []:tp2
Email Address []:
[kylian@proxy ~]$ ls
id_rsa.pub  server.crt  server.key
[kylian@proxy ~]$ sudo mv server.key /etc/pki/tls/private/
[kylian@proxy ~]$ sudo mv server.crt /etc/pki/tls/certs/
[kylian@proxy ~]$ sudo vi /etc/nginx/conf.d/r_proxy.conf
ssl_certificate         /etc/pki/tls/private/server.key;
ssl_certificate_key     /etc/pki/tls/certs/server.crt;
[kylian@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[kylian@proxy ~]$ sudo firewall-cmd --reload
success
```
-------------------------------------------------------------------------------------------------------------------
PC
-------------------------------------------------------------------------------------------------------------------
```bash
C:\Users\kylianartonne>curl -k https://web.tp2.cesi/
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```


