VM Docker
-------------------------------------------------
1.
-------------------------------------------------

```bash
[kylian@docker ~]$ sudo yum install -y yum-utils
[...]
Installed:
  yum-utils-4.0.21-3.el8.noarch

Complete!
[kylian@docker ~]$ sudo yum-config-manager \
>     --add-repo \
>     https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
[kylian@docker ~]$ sudo yum install docker-ce docker-ce-cli containerd.io
Docker CE Stable - x86_64
[...]
Complete!
[kylian@docker ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[kylian@docker ~]$ sudo systemctl start docker
[kylian@docker ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
[...]
[kylian@docker ~]$  sudo docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

[...]

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
[kylian@docker ~]$ sudo usermod -aG docker kylian
[kylian@docker ~]$ grep "kylian" /etc/group
wheel:x:10:kylian
kylian:x:1000:
docker:x:988:kylian
[kylian@docker ~]$ sudo systemctl restart docker
[kylian@docker ~]$ sudo docker info
[kylian@docker ~]$ sudo docker pull nginx
Digest: sha256:9522864dd661dcadfd9958f9e0de192a1fdda2c162a35668ab6ac42b465f0603
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
[kylian@docker ~]$ sudo docker run --name kylian -d -p 8080:80 nginx
fd1798cf3525d9707dad2b90798efd96f673a7541f994cfe442f0c9a3e91f094
[kylian@docker ~]$ # docker run --name kylian -d nginx /bin/sh -c
[kylian@docker ~]$ sudo docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
fd1798cf3525   nginx     "/docker-entrypoint.…"   3 minutes ago   Up 3 minutes   0.0.0.0:8080->80/tcp, :::8080->80/tcp   kylian
[kylian@docker ~]$ sudo mkdir /var/www/
[kylian@docker ~]$ sudo mkdir /var/www/html/
[kylian@docker ~]$ sudo mkdir /var/www/html/cesi/
[kylian@docker ~]$ sudo vi /var/www/html/cesi/index.html
[kylian@docker ~]$ mkdir nginx
[kylian@docker ~]$ cd nginx/
[kylian@docker nginx]$ vi nginx.conf
[kylian@docker nginx]$ docker run -v /home/kylian/nginx/nginx.conf:/etc/nginx/nginx.conf -v /var/www/html/cesi/index.html:/var/www/html/cesi/index.html -d -p
8888:80 nginx
c80dda3e82969ff8a0cf7c6ef01391108341c025a3eec0e64b0ada771350dde2
[kylian@docker nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
c80dda3e8296   nginx     "/docker-entrypoint.…"   3 minutes ago   Up 3 minutes   0.0.0.0:8888->80/tcp, :::8888->80/tcp   nice_haslett
[kylian@docker nginx]$ docker exec -it 45f5bf7f78c0 bash
root@45f5bf7f78c0:/#
[kylian@docker nginx]$ docker inspect c80dda3e8296
[...]
[kylian@docker nginx]$ docker run -d --memory=128M nginx
41618b8ec17b742fc5468428b12c0cd2c457fca548d849d2e30d9d4be2dad284
```
--------------------------------
2.
--------------------------------
```bash
[kylian@docker nginx]$ docker network create wiki
e3f4ffdbe43b715e8e5f78bc1cd5b21de3f4e9bf09470beada668e665558a969
[kylian@docker nginx]$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
c3fcb87206c7   bridge    bridge    local
b59f2ec5c7e0   host      host      local
4c669ce7ff60   none      null      local
e3f4ffdbe43b   wiki      bridge    local
[kylian@docker nginx]$ docker network inspect e3f4ffdbe43b
[
    {
        "Name": "wiki",
[...]
    }
]
[kylian@docker nginx]$ docker run mysql
Unable to find image 'mysql:latest' locally
latest: Pulling from library/mysql
ffbb094f4f9e: Pull complete
df186527fc46: Pull complete
fa362a6aa7bd: Pull complete
5af7cb1a200e: Pull complete
949da226cc6d: Pull complete
bce007079ee9: Pull complete
eab9f076e5a3: Pull complete
8a57a7529e8d: Pull complete
b1ccc6ed6fc7: Pull complete
b4af75e64169: Pull complete
3aed6a9cd681: Pull complete
23390142f76f: Pull complete
Digest: sha256:ff9a288d1ecf4397967989b5d1ec269f7d9042a46fc8bc2c3ae35458c1a26727
Status: Downloaded newer image for mysql:latest
2021-12-09 13:49:38+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 8.0.27-1debian10 started.
2021-12-09 13:49:38+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
2021-12-09 13:49:38+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 8.0.27-1debian10 started.
2021-12-09 13:49:38+00:00 [ERROR] [Entrypoint]: Database is uninitialized and password option is not specified
    You need to specify one of the following:
    - MYSQL_ROOT_PASSWORD
    - MYSQL_ALLOW_EMPTY_PASSWORD
    - MYSQL_RANDOM_ROOT_PASSWORD
[kylian@docker nginx]$ docker run -it -d --name mysql -e MYSQL_ROOT_PASSWORD=123 -e MYSQL_USER=kylian -e MYSQL_PASSWORD=123 -e MYSQL_DATABASE=wiki --network wiki -p 8888:80 mysql
3c1f8a5c96429cb6c782bef00aa25eb454f53ec5c442d0b02387b4ef02d38863
[kylian@docker nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                                        NAMES
3c1f8a5c9642   mysql     "docker-entrypoint.s…"   26 seconds ago   Up 25 seconds   3306/tcp, 33060/tcp, 0.0.0.0:8888->80/tcp, :::8888->80/tcp   mysql
[kylian@docker nginx]$ docker exec -it 3c1f8a5c9642 bash
root@3c1f8a5c9642:/#
root@3c1f8a5c9642:/# mysql -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
mysql> exit
Bye
root@3c1f8a5c9642:/# exit
exit
[kylian@docker nginx]$
[kylian@docker nginx]$ docker pull requarks/wiki
Using default tag: latest
[...]
Status: Downloaded newer image for requarks/wiki:latest
docker.io/requarks/wiki:latest
[kylian@docker nginx]$ docker run -d -p 8080:3000 --name wiki --restart unless-stopped -e "DB_TYPE=mysql" -e "DB_HOST=
localhost" -e "DB_PORT=80" -e "DB_USER=kylian" -e "DB_PASS=123" -e "DB_NAME=mysql" requarks/wiki:2
36f01096fbde0c63a36ed1d6da7a1bac0b365e3103232487b2a06ed8bb4e18ba
[kylian@docker nginx]$ docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED          STATUS          PORTS                                                        NAMES
36f01096fbde   requarks/wiki:2   "docker-entrypoint.s…"   3 seconds ago    Up 2 seconds    3443/tcp, 0.0.0.0:8080->3000/tcp, :::8080->3000/tcp          wiki
3c1f8a5c9642   mysql             "docker-entrypoint.s…"   23 minutes ago   Up 23 minutes   3306/tcp, 33060/tcp, 0.0.0.0:8888->80/tcp, :::8888->80/tcp   mysql
```
